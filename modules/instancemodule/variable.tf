variable "image_id" {
    default = ami-01b799c439fd5516a 
}
variable "key_pair" {
    default = "mykey"
}
variable "instance_type" {
  default = "t2.micro"
}
variable "project" {
    default =  "Devops" 
}
variable "sg_ids" {
    default = "security_group_id"
}
variable "env" {}
variable "subnet_id" {}
