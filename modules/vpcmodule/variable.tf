variable "project" {}
variable "vpc_cidr" {
  default = "10.0.0.0/24"
}
variable "pvt_subnet_cidr" {
  default = "10.0.0.0/25"
}
variable "pub_subnet_cidr" {
  default = "10.0.0.128/25"
}
