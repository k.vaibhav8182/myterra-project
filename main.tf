provider "aws" {
  region = var.region
}

module "my-vpc_module" {
    source = "./modules/vpcmodule"
    project = var.project
    }

module "my-instance" {
    source = "./modules/instancemodule"
    ami_id = var.ami_id
    instance_type = var.instance_type
    sg_ids = var.security_group_id
    project = var.project
    subnet_id = var.pub_subnet_id
    env = var.env
   }

module "my-autoscalling_module"{
  source = "./modules/autoscalingmodule"
  image_id = var.image_id
  instance_type = var.instance_type
  key_pair = var.key_pair
  sg_ids = [aws_security_group.security-group.id]
  project = var.project
  subnet1 = module.my_vpc_module.pvt_subnet_id
  subnet2 = module.my_vpc_module.pub_subnet_id
}
