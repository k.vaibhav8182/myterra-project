# What is Terraform
* HashiCorp Terraform is an infrastructure as code (IaC) software tool that allows DevOps teams to automate infrastructure provisioning using reusable, shareable, human-readable configuration files. The tool can automate infrastructure provisioning in both on-premises and cloud environments.
------------------------------
* (IaC): Infrastructure as code means managing and provisioning of infrastructure on cloud using scripting language HCL (Hashicorp Configuration Language), in short we are implimenting automantion to create infrastructure instend of doing it manualy.
-------------------------
* Terraform is useful for IT professionals, DevOps engineers, cloud engineers, and developers. It allows users to automate the provisioning and management of infrastructure resources across any cloud or data center.
* We can integrate it with multiple cloud providers.

# Why Terraform.
* Cloud Providers having the IAC services, such as AWS have Cloudformation, Azure have Resource manager, GCP have Deployment service, but all this services are limited to that perticular cloud provider. 
* Where Terraform is the open source Tool and compatible with any cloud provider to create and manage infrastructure, using Terraform registry.

# Terraform Installation on Ubuntu
Use documentation for installation of Terraform, as per the OS.
```shell

curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -

#Next, add the official HashiCorp Terraform Linux repository to apt:

sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"

sudo apt-get update && sudo apt-get install terraform

#Once installed, verify the installation:

terraform -v

```
# Write 1st Terraform file
* Hashicorp Configuration language is used written in block by block.
* File ends with `.tf` extention
* multiple file can be executed at a time. 
* we will create multiple files as per requirement and will execute all in single command.
## data types used in file.
1. string --- "suraj", suraj123, abs12sdf,
2. number --- 1, 23, 3242 ,
3. boolean--- true false, 0, 1
4. list --- ["apple", "babana"]
5. map/dictionary-- {
            name = suraj
            city = nagpur
}
* Write sample.tf
```shell
provider "aws" {
    region = "ap-south-1"       #`This block tells Terraform that we are working with AWS cloud`
}
 # It uses Terraform registory to connect with cloud providers.
```
* Now crete AWS resources
```shell
# Resource block
resource "aws_instance" "my_instance" {
           #aws_instance-->resource name   "my_instance" --> reference name for terraform
    ami: #use ami id
    instance_type: #use instance type eg. t2.micro
    key_name: #use key pair name that you are using.
      
}
``` 

# teraform lifecycle
1. init ------- initiating all configuration files 
2. plan ------- blueprints 
3. apply ------ runing / execute all confuguration files
4. destroy ---- deleting all infra



